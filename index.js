let x;
x = prompt('Введите целое число: ');

while (x == '') {
    x = prompt('Вы не ввели число! Введите целое число:')
}

while (x % 1 !== 0) {
    x = prompt('Вы ввели не целое число! Введите целое число: ', x);
}

while (x < 0) {
    x = prompt('Вы ввели отрицательное число! Введите положительное число:', x)
}

for (let i = 0 ; i <= x; i++) {
    if (i == 0 && x >= 0 && x < 5) {
        alert('Sorry, no numbers!')
    } else if (i % 5 == 0 && i > 0) {
        alert(i);
    }  
}

let m, n;
    m = prompt('Введите начало: ');
    n = prompt('Введите конец: ');
while (m == 0 || n == 0 || m > n || m === '' || n === '' || m < 0 || n < 0) {
    alert('Не корректно указаны числа!');
    m = prompt('Введите начало: ');
    n = prompt('Введите конец: ');
}
nextPrime:
for (let i = m; i <= n; i++) {
    for (let j = 2; j < i; j++) {
        if (i % j == 0) continue nextPrime; 
    }
    console.log(i);
}